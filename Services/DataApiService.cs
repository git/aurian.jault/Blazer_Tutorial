﻿using Blazor.Component;
using Blazor.Factories;
using Blazor.Models;

namespace Blazor.Services
{
    public class DataApiService : IDataService
    {
        private readonly HttpClient _http;

        public DataApiService(
            HttpClient http)
        {
            _http = http;
        }

        public async Task Add(ItemModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            // Save the data
            await _http.PostAsJsonAsync("https://codefirst.iut.uca.fr/containers/container-blazor-web-api-julienriboulet/api/Crafting/", item);
        }

        public async Task<int> Count()
        {
            return await _http.GetFromJsonAsync<int>("https://codefirst.iut.uca.fr/containers/container-blazor-web-api-julienriboulet/api/Crafting/count");
        }

        public async Task<List<Item>> List(int currentPage, int pageSize)
        {
            return await _http.GetFromJsonAsync<List<Item>>($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-julienriboulet/api/Crafting/?currentPage={currentPage}&pageSize={pageSize}");
        }

        public async Task<Item> GetById(int id)
        {
            return await _http.GetFromJsonAsync<Item>($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-julienriboulet/api/Crafting/?currentPage={id}");
        }

        public async Task Update(int id, ItemModel model)
        {
            // Get the item
            var item = ItemFactory.Create(model);

            await _http.PutAsJsonAsync($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-julienriboulet/api/Crafting/{id}", item);
        }

        public async Task Delete(int id)
        {
            await _http.DeleteAsync($"https://codefirst.iut.uca.fr/containers/container-blazor-web-api-julienriboulet/api/Crafting/{id}");
        }

        public async Task<List<CraftingRecipe>> GetRecipes()
        {
            return await _http.GetFromJsonAsync<List<CraftingRecipe>>("https://codefirst.iut.uca.fr/containers/container-blazor-web-api-julienriboulet/api/Crafting/");
        }
    }
}
